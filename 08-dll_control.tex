\section{DLL control}

This block contains the possible Dynamic Link Library formats accessible for the user. The Dll’s are mainly used to control the turbine speed and pitch, but since the DLL format is very general, other use is possible too e.g. external loading of the turbine. Since the HAWC2 core has no information about external stiffness or inertia we have experienced some issues with the solver if the DLL includes high stiffness terms or especially large inertia terms. The new type2\_dll interface is slightly more stable related to the solver than the hawc\_dll interface.

\subsection{Main command block – dll}

So far only one DLL format is available, which is the hawc\_dll format listed below.

\subsection{Sub command block – hawc\_dll}

In the HAWC\_DLL format a subroutine within an externally written DLL is setup. In this subroutine call two one-dimensional arrays are transferred between the HAWC2 core and the DLL procedure. The first contains data going from the HAWC2 core to the DLL and the other contains data going from the DLL to the core. It is very important to notice that the data are transferred between HAWC2 and the DLL in every timestep and every iteration. The user should handle the iteration inside the DLL.

Two more subroutines are called if they are present inside the dll file: 

The first is an initialisation call including a text string written in the init\_string in the commands below. This could be the name of a file holding local input parameters to the data transfer subroutine. This call is only performed once. The name of this subroutine is the same name as the data transfer subroutine defined with the command dll\_subroutine below with the extra name ‘\_init’, hence is the data transfer subroutine is called ‘test’, the initialisation subroutine will be ‘test\_init’.

The second subroutine is a message exchange subroutine, where messages written in the DLL can be send to the HAWC2 core for logfile writing. The name of this subroutine is the same name as the data transfer subroutine defined with the command dll\_subroutine below with the extra name ‘\_message’, hence is the data transfer subroutine is called ‘test’, the initialisation subroutine will be ‘test\_message’.

The command block can be repeated as many times as desired. Reference number to DLL is same order as listed, starting with number 1. However it is recommended to refer the DLL using the name feature which in many cases can avoid confusion.

\begin{tabular}{|p{0.5cm}|p{4cm}|p{8cm}|}
\hline
Obl. & Command name & Explanation\\
\hline
 & name & 1. Reference name of this DLL (to be used with DLL output commands)\\
\hline
* & filename & 1. Filename incl. relative path of the DLL\\
 &  & (example ./DLL/control.dll)\\
\hline
* & dll\_subroutine & 1. Name of subroutine in DLL that is addressed (remember to specify the name in the DLL with small letters!)\\
\hline
* & arraysizes & 1. size of array with outgoing data\\
 &  & 2. size of array with ingoing data\\
\hline
 & deltat & 1. Time between dll calls. Must correspond to the simulation sample frequency or be a multiple of the time step size. If deltat=0.0 or the deltat command line is omitted the HAWC2 code calls the dll subroutine at every time step.\\
\hline
 & init\_string & 1. Text string (max 256 characters) that will be transferred to the DLL through the subroutine ‘subroutine\_init’. Subroutine is the name given in in the command dll\_subroutine. No blanks can be included.\\
\hline
\end{tabular}


\subsection{Sub command block – type2\_dll}

This dll interface is an updated slightly modified version of the hawc\_dll interface. In the TYPE2\_DLL format a subroutine within an externally written DLL is setup. In this subroutine call two one-dimensional arrays are transferred between the HAWC2 core and the DLL procedure. The first contains data going from the HAWC2 core to the DLL and the other contains data going from the DLL to the core. It is very important to notice that the data are transferred between HAWC2 and the DLL in the first call of every timestep where the out-going variables are based on last iterated values from previous time step. The sub command output and actions are identical for both the hawc\_dll and the type2\_dll interfaces.

In the dll connected with using the type2\_dll interface two subroutines should be present. An initialization routine called only once before the time simulation begins, and an update routine called in every time step. The format in the calling of these two subroutines are identical where two arrays of double precision is exchanged. The subroutine uses the cdecl calling convention.

\begin{tabular}{|p{0.5cm}|p{4cm}|p{8cm}|}
\hline
Obl. & Command name & Explanation\\
\hline
 & name & 1. Reference name of this DLL (to be used with DLL output commands)\\
\hline
* & filename & 1. Filename incl. relative path of the DLL\\
 &  & (example ./DLL/control.dll)\\
\hline
* & dll\_subroutine\_init & 1. Name of initialization subroutine in DLL that is addressed (remember to specify the name in the DLL with small letters!)\\
\hline
* & dll\_subroutine\_update & 1. Name of subroutine in DLL that is addressed at every time step (remember to specify the name in the DLL with small letters!)\\
\hline
* & arraysizes\_init & 1. size of array with outgoing data in the initialization call\\
 &  & 2. size of array with ingoing data in the initialization call\\
\hline
* & arraysizes\_update & 1. size of array with outgoing data in the update call\\
 &  & 2. size of array with ingoing data in the update call\\
\hline
 & deltat & 1. Time between dll calls. Must correspond to the simulation sample frequency or be a multiple of the time step size. If deltat=0.0 or the deltat command line is omitted the HAWC2 code calls the dll subroutine at every time step.\\
\hline
\end{tabular}

when using the type2\_dll interface the values transferred to the DLL in the initialization phase is done using a sub command block called init. The commands for this subcommand block is identical to the output subcommand explained below, but only has the option of having the constant output sensor available. An example is given for a small dll that is used for converting rotational speed between high speed and low speed side of a gearbox.:

\begin{verbatim}
begin dll;
  begin type2_dll;
    name hss_convert;
    filename  ./control/hss_convert.dll ;
    arraysizes_init  3 1 ;
    arraysizes_update  2 2 ;
    begin init;
      constant 1 2.0 ;     number of used sensors - in this case only 1
      constant 2 35.110;   gearbox ratio
      constant 3 35.110;   gearbox ratio
    end init;
    begin output;
      constraint bearing1 shaft_rot 2 only 2 ;   rotor speed in rpm     
      constraint bearing1 shaft_rot 3 only 2 ;   rotor speed in rad/s     
    end output;
;
    begin actions;    
;      rotor speed in rpm * gear_ratio
;      rotor speed in rad/s * gear_ratio
    end actions;
  end type2_dll;
end dll;

\end{verbatim}

\subsection{Sub command block – init}

In this block type2\_dlls can be initialized by passing constants to specific channels.

\begin{tabular}{|p{0.5cm}|p{4cm}|p{8cm}|}
\hline
Obl. & Command name & Explanation\\
\hline
* & constant & Constants passed to the dll.\\
 &  & 1. Channel number\\
 &  & 2. Constant value\\
\hline
\end{tabular}

\subsection{Sub command block – output}

In this block the same sensors are available as when data results are written to a file with the main block command output, see page xxx++. The order of the sensors in the data array is continuously increased as more sensors are added.
 
\subsection{Sub command block – actions}

In this command block variables inside the HAWC2 code is changed depending of the specifications. This command block can be used for the hawc\_dll interface as well as the type2\_dll interface. An action commands creates a handle to the HAWC2 model to which a variable in the input array from the DLL is linked.

!NB in the command name two separate words are present.

\begin{tabular}{|p{0.5cm}|p{4cm}|p{8cm}|}
\hline
Obl. & Command name & Explanation\\
\hline
 & aero beta & The flap angle beta is set for a trailing edge flap section (is the mhhmagf stall model is used). The angle is positive towards the pressure side of the profile. Unit is [deg]\\
 &  & 1. Blade number\\
 &  & 2. Flap section number\\
\hline
 & aero bem\_grid\_a & 1. Number of points\\
\hline
 & body force\_ext & An external force is placed on the structure. Unit is [N].\\
 &  & 1. body name\\
 &  & 2. node number\\
 &  & 3. componet (1 = Fx, 2 = Fy, 3 = Fz)\\
\hline
 & body moment\_ext & An external moment is placed on the structure. Unit is [Nm].\\
 &  & 1. body name\\
 &  & 2. node number\\
 &  & 3. component (1 = Mx, 2 = My, 3 = Mz)\\
\hline
 & body force\_int & An external force with a reaction component is placed on the structure. Unit is [N].\\
 &  & 1. body name for action force\\
 &  & 2. node number\\
 &  & 3. component (1 = Fx, 2 = Fy, 3 = Fz)\\
 &  & 4. body name for reaction force\\
 &  & 5. Node number\\
\hline
 & body moment\_int & An external moment with a reaction component is placed on the structure. Unit is [N].\\
 &  & 1. body name for action moment\\
 &  & 2. node number\\
 &  & 3. component (1 = Mx, 2 = My, 3 = Mz) \\
 &  & 4. body name for reaction moment\\
 &  & 5. Node number\\
\hline
 & body bearing\_angle & A bearing either defined through the new structure format through bearing2 or through the old structure format (spitch1=pitch angle for blade 1, spitch2=pitch angle for blade 2,...). The angle limits are so far [0-90deg].\\
 &  & 1. Bearing name\\
\hline
 & mbdy force\_ext & An external force is placed on the structure. Unit is [N].\\
 &  & 1. main body name\\
 &  & 2. node number on main body\\
 &  & 3. component (1 = Fx, 2 = Fy, 3 = Fz) , if negative number the force is inserted with opposite sign.\\
 &  & 4. coordinate system (possible options are: mbdy name,”global”,”local”). “local” means local element coo on the inner element (on the element indexed 1 lower that the node number). One exception if node number =1 then the element nr. also equals 1.\\
\hline
\end{tabular}

\begin{tabular}{|p{0.5cm}|p{4cm}|p{8cm}|}
\hline
 & mbdy moment\_ext & An external moment is placed on the structure. Unit is [Nm].\\
 &  & 1. main body name\\
 &  & 2. node number on main body\\
 &  & 3. component (1 = Mx, 2 = My, 3 = Mz) , if negative number the moment is inserted with opposite sign.\\
 &  & 4. coordinate system (possible options are: mbdy name,”global”,”local”). “local” means local element coo on the inner element (on the element indexed 1 lower that the node number). One exception if node number =1 then the element nr. also equals 1.\\
\hline
 & mbdy force\_int & An internal force with a reaction component is placed on the structure. Unit is [N].\\
 &  & 1. main body name for action force\\
 &  & 2. node number on main body\\
 &  & 3. component (1 = Fx, 2 = Fy, 3 = Fz) , if negative number the force is inserted with opposite sign.\\
 &  & 4. coordinate system (possible options are: mbdy name,”global”,”local”). “local” means local element coo on the inner element (on the element indexed 1 lower that the node number). One exception if node number =1 then the element nr. also equals 1.\\
 &  & 5. main body name for reaction force\\
 &  & 6. Node number on this main body\\
\hline
 & mbdy moment\_int & An internal force with a reaction component is placed on the structure. Unit is [Nm].\\
 &  & 1. main body name for action moment\\
 &  & 2. node number on main body\\
 &  & 3. component (1 = Mx, 2 = My, 3 = Mz) , if negative number the moment is inserted with opposite sign.\\
 &  & 4. coordinate system (possible options are: mbdy name,”global”,”local”). “local” means local element coo on the inner element (on the element indexed 1 lower that the node number). One exception if node number =1 then the element nr. also equals 1.\\
 &  & 5. main body name for reaction moment\\
 &  & 6. Node number on this main body\\
\hline
 & constraint bearing2 angle\_deg & The angle of a bearing2 constraint is set. The angle limits are so far [+/-90deg].\\
 &  & 1. Bearing name\\
\hline
 & constraint bearing3 angle\_deg & The angle of a bearing3 constraint is set. The angle limits are so far [+/-90deg].\\
 &  & 1. Bearing name\\
\hline
 & constraint bearing3 omegas & The angular velocity of a bearing3 constraint is set. \\
 &  & 1. Bearing name\\
\hline
 & body printvar & Variable is just echoed on the screen. No parameters.\\
\hline
 & body ignore & 1. Number of consecutive array spaces that will be ignored\\
\hline
 & mbdy printvar & Variable is just echoed on the screen. No parameters.\\
\hline
 & mbdy ignore & 1. Number of consecutive array spaces that will be ignored\\
\hline
 & general printvar & Variable is just echoed on the screen. No parameters.\\
\hline
 & general ignore & 1. Number of consecutive array spaces that will be ignored\\
\hline
\end{tabular}

\begin{tabular}{|p{0.5cm}|p{4cm}|p{8cm}|}
\hline
 & stop\_simulation & Logical switch. If value is 1 the simulation will be stopped and output written.\\
\hline
 & wind printvar & Variable is just echoed on the screen. No parameters.\\
\hline
 & wind windspeed\_u & External contribution to wind speed in u-direction [m/s]\\
\hline
 & wind winddir & External contribution to the wind direction (turb. box is also rotated) [°]\\
\hline
 & quake comp & 1. Degree of freedom\\
\hline
 & ext\_sys control & 1. Name of external system\\
\hline
\end{tabular}
\pagebreak
\subsection{HAWC\_DLL format example written in FORTRAN 90}

\begin{verbatim}
subroutine test(n1,array1,n2,array2)
implicit none
!DEC$ ATTRIBUTES DLLEXPORT, ALIAS:'test'::test
integer*4           :: n1, &    ! Dummy integer value containing the array size of array1
                       n2       ! Dummy integer value containing the array size of array2
real*4,dimension(10) :: array1  ! fixed-length array, data from HAWC2 to DLL 
                                ! – in this case with length 10
real*4,dimension(5)  :: array2  ! fixed-length array, data from DLL to HAWC2 
                                ! – in this case with length 5

! Code is written here

end subroutine test

!-------------------------------------------------------

Subroutine test_init(string256)
Implicit none
!DEC$ ATTRIBUTES DLLEXPORT, ALIAS:'test_init'::test_init
Character*256 :: string256

! Code is written here

End subroutine test_init

!-------------------------------------------------------

Subroutine test_message(string256)
Implicit none
!DEC$ ATTRIBUTES DLLEXPORT, ALIAS:'test_message'::test_message
Character*256 :: string256

! Code is written here

End subroutine test_message

\end{verbatim}
\pagebreak
\subsection{HAWC\_DLL format example written in Delphi / Lazarus / Pascal}

\begin{verbatim}
library test_dll;

type
  array_10 = array[1..10] of single;
  array_5  = array[1..5] of single;
  ts       = array[0..255] of char;

Procedure test(var n1:integer;var array1 : array_10;
               var n2:integer;var array2 : array_5);stdcall;
// n1 is a dummy integer value containing the size of array1
// n2 is a dummy integer value containing the size of array2
begin
  // Code is written here

end;

//----------------------------------------------------------

Procedure test_init(var string256:ts; length:integer);stdcall;
var
  init_str:string[255]
begin
  init_str=strpas(string256);
  // Code is written here
  writeln(init_str);
end;

//----------------------------------------------------------

Procedure test_message(var string256:ts; length:integer);stdcall;
var
  message_str:string;
begin
  // Code is written here
  message_str:=’This is a test message’;
  strPCopy(string256,message_str);
end;

exports test,test_init,test_message;

begin
  writeln('The DLL pitchservo.dll is loaded with succes');

  // Initialization of variables can be performed here
end;

end.

\end{verbatim}

\pagebreak
\subsection{HAWC\_DLL format example written in C}

\begin{verbatim}
extern "C" void __declspec(dllexport) __cdecl test(int &size_of_Data_in,
float Data_in[], int &size_of_Data_out, float Data_out[])
{
 for (int i=0; i<size_of_Data_out; i++) Data_out[i]=0.0;
//
 printf("size_of_Data_in  %d: \n",size_of_Data_in);
 printf("Data_in          %g: \n",Data_in[0]);
 printf("size_of_Data_out %d: \n",size_of_Data_out);
 printf("Data_out         %g: \n",Data_out[0]);
 
}

extern "C" void __declspec(dllexport) __cdecl test_init(char* pString, int length)
{
	// Define buffer (make room for NULL-char)
	const int max_length = 256;
	char buffer[max_length+1];
	//
	// Print the length of pString
	printf("test_init::length = %d\n",length);
	//
	// Transfer string
	int nchar = min(max_length, length);
	memcpy(buffer, pString, nchar);
	//
	// Add NULL-char
	buffer[nchar] = '\0';
	//
	// Print it...
	printf("%s\n",buffer);
}

extern "C" void __declspec(dllexport) __cdecl test_message(char* pString, int max_length)
{
	// test message (larger than max_length)
	char pmessage[] = "This is a test message "
			 "and it continues and it continues and it continues "
			 "and it continues and it continues and it continues "
			 "and it continues and it continues and it continues "
			 "and it continues and it continues and it continues "
			 "and it continues and it continues and it continues "
			 "and it continues and it continues and it continues ";

	// Check max length  -  transfer only up to max_length number of chars
	int nchar = min((size_t)max_length, strlen(pmessage));  // nof chars to transfer                
    // (<= max_length)
	memcpy(pString, pmessage, nchar);
	//
	// Add NULL-char if string space allows it (FORTRAN interprets a NULL-char as 
// the end of the string)
	if (nchar < max_length) pString[nchar] = '\0';
}
\end{verbatim}

\pagebreak
\subsection{TYPE2\_dll written in Delphi / Lazarus / Delphi}

\begin{verbatim}
library hss_convert;

uses
  SysUtils,
  Classes,
  Dialogs;

Type
  array_1000 = array[0..999] of double;
Var
  factor : array of double;
  nr : integer;
{$R *.res}

procedure initialize(var InputSignals: array_1000;var OutputSignals: array_1000); cdecl;
var
  i : integer;
begin
  nr:=trunc(inputsignals[0]);
  if nr>0 then begin
    setlength(factor,nr);
    for i:=1 to nr do
      factor[i-1]:=Inputsignals[i];
    outputsignals[0]:=1.0;
  end else outputsignals[0]:=0.0;
end;

procedure update(var InputSignals: array_1000;var OutputSignals: array_1000); cdecl;
var
  i : integer;
begin
  for i:=0 to nr-1 do begin
    OutputSignals[i] := InputSignals[i]*factor[i];
  end;
end;

exports Initialize,Update;

begin
  // Main body

end.
 
 

\end{verbatim}

\pagebreak
\subsection{TYPE2\_dll written in C}

\begin{verbatim}
extern "C" void __declspec(dllexport) __cdecl initialize(dfloat *Data_in, dfloat *Data_out) 
{ for (int i=0; i<8; i++) Data_out[0]+=Data_in[i]; 
}

extern "C" void __declspec(dllexport) __cdecl update(dfloat *Data_in, dfloat *Data_out) 
{ for (int i=0; i<25; i++) Data_out[0]+=Data_in[i]; 
Data_out[8]=123;
}

\end{verbatim}

\pagebreak
\subsection{TYPE2\_DLL format example written in FORTRAN 90}

\begin{verbatim}
subroutine initialize(array1,array2)
implicit none
!DEC$ ATTRIBUTES DLLEXPORT, C, ALIAS:'initialize'::initialize
real*8,dimension(1000) :: array1  ! fixed-length array, data from HAWC2 to DLL 
                                ! – in this case with length 1000
real*8,dimension(1)    :: array2  ! fixed-length array, data from DLL to HAWC2 
                                ! – in this case with length 1

! Code is written here

end subroutine initialize

!-------------------------------------------------------

subroutine update(array1,array2)
implicit none
!DEC$ ATTRIBUTES DLLEXPORT, C, ALIAS:'update'::update
real*8,dimension(1000) :: array1  ! fixed-length array, data from HAWC2 to DLL 
                                ! – in this case with length 1000
real*8,dimension(100)    :: array2  ! fixed-length array, data from DLL to HAWC2 
                                ! – in this case with length 100

! Code is written here

end subroutine initialize
 

\end{verbatim}


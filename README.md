[![pipeline status](https://gitlab.windenergy.dtu.dk/mikf/HAWC2-Manual/badges/master/pipeline.svg)](https://gitlab.windenergy.dtu.dk/mikf/HAWC2-Manual/commits/master)

Welcome to the HAWC2 Manual
------------------

Check out the newest development version here:
[HAWC2 Manual](https://mikf.pages.windenergy.dtu.dk/HAWC2-Manual).

The official releases can be downloaded here:
[HAWC2 Manual](http://www.hawc2.dk/download/hawc2-manual).
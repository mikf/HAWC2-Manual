# -*- coding: utf-8 -*-
"""
Created on Wed Nov  7 14:50:30 2018

@author: mikf
"""
import pandas as pd
import numpy as np
path = '7-beam_struct_data.xlsx'
df = pd.read_excel(path).astype('str')
df = df.replace(np.nan, '', regex=True)
cs = list(df)[:-1]
last = list(df)[-1]
N = len(cs)+2
adjustments = ('|'+N*'l|')
#last_width = 10
#print(r'\begin{table}[]')
print(r'\begin{tabular}{'+adjustments+'}')
print(r'\hline')
line = ' & '.join([c for c in cs])
line += r' & \multicolumn{2}{p{10cm}|}{\raggedright '
line += last + r'}\\'
line = line.replace('_','\_')
print(line)

for index, row in df.iterrows():
    try:
        if not row[cs[1]] == '':
            print(r'\hline')
    except:
         print(r'\hline')
    line = ' & '.join([row[c] for c in cs])
    line += r' & \multicolumn{2}{p{10cm}|}{\raggedright '
    line += row[last] + r'}\\'
    line = line.replace('_','\_')
    print(line)
print(r'\hline')
print('\end{tabular}')
#print('\end{table}')
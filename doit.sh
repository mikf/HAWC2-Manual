#!/bin/bash

# 'GLOBAL VARS'
destinationfolder="$DESTINATIONFOLDER"

case "$1" in
prepare)
##################################################################
### PREPARE
##################################################################
echo
echo "++ PREPARE ++"
echo

# CONFIG/VARS
errors=0

echo
echo "++ Install Latex ++"
echo

#
# INSTALL
#

echo "APT-GET UPDATE:"
apt-get update
echo
echo "APT-GET INSTALL texlive"
apt-get install --yes texlive
exitcode=$?
if [ $exitcode != 0 ] ; then exit $exitcode ; fi

echo
echo "APT-GET INSTALL texlive-lang-german"
apt-get install --yes texlive-lang-german
exitcode=$?
if [ $exitcode != 0 ] ; then exit $exitcode ; fi

echo
echo "APT-GET INSTALL texlive-lang-english"
apt-get install --yes texlive-lang-english
exitcode=$?
if [ $exitcode != 0 ] ; then exit $exitcode ; fi

if [ $2 == true ] ; then
    echo
    echo "APT-GET INSTALL texlive-bibtex-extra"
    apt-get install --yes texlive-bibtex-extra
    exitcode=$?
    if [ $exitcode != 0 ] ; then exit $exitcode ; fi
fi

echo
echo "APT-GET INSTALL texlive-latex-extra"
apt-get install --yes texlive-latex-extra
exitcode=$?
if [ $exitcode != 0 ] ; then exit $exitcode ; fi

# echo
# echo "APT-GET INSTALL curl"
# apt-get install --yes curl
# exitcode=$?
# if [ $exitcode != 0 ] ; then exit $exitcode ; fi

#
# TESTCALLS
#

echo
echo "++ Testcalls ++"
echo
echo "++ PDFLATEX:"
pdflatex --version
exitcode=$?
if [ $errors == 0 -a $exitcode != 0 ] ; then errors=$exitcode ; fi

if [ $2 == true ] ; then
    echo
    echo "++ BIBTEX:"
    bibtex --version
    exitcode=$?
    if [ $errors == 0 -a $exitcode != 0 ] ; then errors=$exitcode ; fi
fi

# echo
# echo "++ CURL:"
# curl --version
# exitcode=$?
# if [ $errors == 0 -a $exitcode != 0 ] ; then errors=$exitcode ; fi

exit $errors

;;

runlatex)
##################################################################
### RUNLATEX
##################################################################
echo
echo "++ RUNLATEX ++"
echo

# CONFIG/VARS
errors=0
filenames=""
usebibtex=false
usebibtex=$2

# LOAD ALL FILES
n=0
for arg in "$@" ; do
    if [ $n -ge 2 ] ; then # >=
        filenames="${filenames} ${arg}"
        echo "Will process file: ${arg}"
    fi
    n=$((n+1))
done

# CODE
echo
echo "++ START BUILDING LATEX ++"
echo
echo "CREATE OUTPUTFOLDER: ${destinationfolder}"
mkdir -p $destinationfolder # -p to only mkdir if it does not exist already
# echo "Im here: $(pwd)"
# ls -la

function run_pdflatex_cmd {
    pdflatex -output-directory $destinationfolder $filenames
    exitcode=$?
    if [ $errors == 0 -a $exitcode != 0 ] ; then errors=$exitcode ; echo " HERE WAS THE ERROR "; echo ; fi
}

function run_bibtex_cmd {
    bibtex $filenames
    exitcode=$?
    if [ $errors == 0 -a $exitcode != 0 ] ; then errors=$exitcode ; echo " HERE WAS THE ERROR "; echo ; fi
}

echo
echo "1. ROUND PDFLATEX:"
run_pdflatex_cmd

if [ $usebibtex == true ] ; then
    echo
    echo "1. ROUND BIBTEX:"
    run_bibtex_cmd
fi

echo
echo "2. ROUND PDFLATEX:"
run_pdflatex_cmd

if [ $usebibtex == true ] ; then
    echo
    echo "2. ROUND BIBTEX:"
    run_bibtex_cmd
fi

echo
echo "3. ROUND PDFLATEX:"
run_pdflatex_cmd

echo
echo "4. ROUND PDFLATEX:"
run_pdflatex_cmd

echo
echo "++ FINISHED BUILDING LATEX ++"

exit $errors

;;

publish)
##################################################################
### PUBLISH
##################################################################
echo
echo "++ PUBLISH ++"
echo

echo "++ PUBLISH PDF ++"
echo "do smtg"
echo "CI_SERVER_NAME: ${CI_SERVER_NAME}"
echo "CI_BUILD_REF: ${CI_BUILD_REF}"
echo "CI_BUILD_REF_NAME: ${CI_BUILD_REF_NAME}"
echo "CI_BUILD_ID: ${CI_BUILD_ID}"
echo "CI_BUILD_REPO: ${CI_BUILD_REPO}"
echo "CI_PROJECT_ID: ${CI_PROJECT_ID}"
echo "CI_PROJECT_DIR: ${CI_PROJECT_DIR}"
echo
echo "Calling API"
echo
echo "get 20 last commits"
curl -s -H "PRIVATE-TOKEN: mXz-wWhYczMnACjJGWyz" "https://gitlab.com/api/v3/projects/${CI_PROJECT_ID}/repository/commits" \
| bash build_script/json.sh -l -n
echo
echo "++ PDF PUBLISHED ++"

;;

clean)
##################################################################
### CLEAN
##################################################################
echo
echo "++ CLEAN ++"
echo

# VARS
errors=0

echo "++ CLEANUP LATEX ++"
# ls -la

echo "Removing *.aux|bbl|blg|dvi|log|synctex.gz|toc"
rm --force *.aux *.bbl *.blg *.dvi *.log *.synctex.gz *.toc
exitcode=$?
if [ $errors == 0 -a $exitcode != 0 ] ; then errors=$exitcode ; fi

echo "Removing in ${destinationfolder} *.aux|bbl|blg|dvi|log|synctex.gz|toc"
cd ${destinationfolder} \
&& rm --force *.aux *.bbl *.blg *.dvi *.log *.synctex.gz *.toc \
&& cd ..
exitcode=$?
if [ $errors == 0 -a $exitcode != 0 ] ; then errors=$exitcode ; fi

# ls -la
# ls -la "${destinationfolder}/"
echo "++ CLEANUP COMPLETE ++"

exit $errors

;;

buildpages)
##################################################################
### BUILD PAGES
##################################################################
echo
echo "++ BUILD PAGES ++"
echo

function gethtml {
    html='For redirect click here'
    echo $html
}

# VARS
userorgroup="aeimer"
projectname="praktikumsbericht_ss2016"
urlbrowse="https://gitlab.com/${userorgroup}/${projectname}/builds/${CI_BUILD_ID}/artifacts/browse/${destinationfolder}"
urlfile="https://gitlab.com/${userorgroup}/${projectname}/builds/${CI_BUILD_ID}/artifacts/file/${destinationfolder}"

# CODE
echo "Add folder .public"
mkdir .public
cd .public

echo "Add folder redir"
mkdir redir
cd redir

echo "Add REDIR to root of artifacts"
touch index.html
gethtml $urlbrowse > index.html

# ITERATE OVER ALL FILES
n=0
for arg in "$@" ; do
    if [ $n -ge 1 ] ; then # >=
        echo "Processing file: ${arg}"
        mkdir $arg
        cd $arg
        touch index.html
        gethtml "${urlfile}/${arg}.pdf" > index.html
        cd ..
    fi
    n=$((n+1))
done


;;

*)
##################################################################
### HELP / EVERYTHING ELSE
##################################################################
echo
echo "++ HELP ++"
echo

echo "PREPARE: ${$0} prepare [true|false]"
echo "PREPARE: ${$0} runlatex [true|false] Files..."
echo "PREPARE: ${$0} publish"
echo "PREPARE: ${$0} clean"
echo "PREPARE: ${$0} help"

exit

;;
esac